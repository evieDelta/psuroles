package main

import (
	"flag"
	"log"

	"git.lan/psuroles/config"
	"git.lan/psuroles/gcmd"
	"git.lan/psuroles/module"
	"git.lan/psuroles/tagger"
)

// main, starts stuff
func main() {
	modules := []module.Module{
		gcmd.Module,
		tagger.Module,
		helpmod,
	}

	launch(modules)
	startUntilStop()
}

// some initialisation stuff, these are done within init because they are expected to be always ready
func init() {
	// set the config file location (it has some defaults to check so you don't need to specify with the flag if its in ./config.toml or ./data/config.toml)
	flag.StringVar(&flagConfig, "config", "", "the config file to use")
	flag.Parse()

	// load le config
	cfg, err := config.AConf(flagConfig)
	if err != nil {
		log.Fatal("Configuration error", err)
	}
	conf = cfg
}
