package tagger

import "fmt"

// NewTag adds a tag to the system
func NewTag(guild, name string) (uint64, error) {
	tag := Tag{
		Name: name,
		ID:   <-ints,
	}

	serverMutex.Lock()
	defer serverMutex.Unlock()

	serverStash[guild].Tags[tag.ID] = &tag

	return tag.ID, nil // error type added in api in advance so transition to database is smoother
}

// GetTag obtains a tag from the system
func GetTag(guild string, id uint64) (Tag, error) {
	serverMutex.Lock()
	defer serverMutex.Unlock()

	if serverStash[guild].Tags[id] == nil {
		return Tag{}, fmt.Errorf("Tag %v not found in guild %v", id, guild)
	}
	tag := serverStash[guild].Tags[id]

	return *tag, nil
}

// GetAllTags gets all tags for a guild from the system
func GetAllTags(guild string) ([]Tag, error) {
	serverMutex.Lock()
	defer serverMutex.Unlock()

	tags := serverStash[guild].Tags

	var taglist = make([]Tag, 0, len(tags))

	for _, x := range tags {
		taglist = append(taglist, *x)
	}

	return taglist, nil
}

// DelTag deletes a tag from the system
func DelTag(guild string, id uint64) error {
	serverMutex.Lock()
	defer serverMutex.Unlock()

	if serverStash[guild].Tags[id] == nil {
		return nil
	}
	delete(serverStash[guild].Tags, id)

	return nil
}

// AddUserTag adds a tag to a user
func AddUserTag(guild, user string, id uint64) (bool, error) {
	tag, err := GetTag(guild, id)
	if err != nil {
		return false, err
	}

	userMutex.Lock()
	defer userMutex.Unlock()

	userNilCheck(guild, user)
	userStash[guild+":"+user].Tags[tag.ID] = true

	return true, nil // error type added in api in advance so transition to database is smoother
}

// GetUserTags gets a users tags
func GetUserTags(guild, user string) ([]uint64, error) {
	userMutex.Lock()
	defer userMutex.Unlock()

	userNilCheck(guild, user)
	tags := userStash[guild+":"+user].Tags

	var taglist = make([]uint64, 0, len(tags))

	for x := range tags {
		taglist = append(taglist, x)
	}

	return taglist, nil
}

// GetUserTagsInfo gets a users tags but with the tag info
func GetUserTagsInfo(guild, user string) ([]Tag, error) {
	tags, err := GetUserTags(guild, user)
	if err != nil {
		return nil, err
	}

	var taglist = make([]Tag, 0, len(tags))

	for _, t := range tags {
		tag, err := GetTag(guild, t)
		if err != nil {
			return nil, err
		}
		taglist = append(taglist, tag)
	}

	return taglist, nil
}

// RemUserTag removes a tag from a user
func RemUserTag(guild, user string, id uint64) error {
	userMutex.Lock()
	defer userMutex.Unlock()

	userNilCheck(guild, user)
	userStash[guild+":"+user].Tags[id] = false

	return nil
}
