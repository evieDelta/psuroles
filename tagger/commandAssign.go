package tagger

import (
	"fmt"
	"strconv"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

// CAssign assigns a tag to the calling user
var CAssign = &drc.Command{
	Name:         "assign",
	Manual:       []string{"assign an existing tag to yourself, read ``.prototype help`` for important notes about the current prototype status"},
	CommandPerms: discordgo.PermissionSendMessages,
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 1,
	},
	Exec: fAssign,
}

func fAssign(ctx *drc.Context) error {
	if ctx.Mes.GuildID == "" {
		return ctx.Reply("Direct Messages are unsupported")
	}

	// Parse integer
	id, err := strconv.ParseUint(ctx.Args[0].Raw, 0, 0)
	if err != nil {
		return drc.NewParseError(err, "Invalid numarical ID")
	}

	// get tag as to be able to use the name in response
	tag, err := GetTag(ctx.Mes.GuildID, id)
	if err != nil {
		return drc.NewFailure(err, "Error finding tag")
	}

	// add tag to user
	ok, err := AddUserTag(ctx.Mes.GuildID, ctx.Mes.Author.ID, id)
	if err != nil {
		return drc.NewFailure(err, "Error assigning tag")
	}

	// create response (due to current design this is redundent with the above error check
	// as addusertag only ever returns false if an error occoured,
	// however in the future if things like assignment requirements are added this may be used)
	var response string
	if ok {
		response = fmt.Sprintf("%v assigned tag %v", ctx.Mes.Author.String(), tag.Name)
	} else {
		response = fmt.Sprintf("could not assign tag %v due to unknown reason", tag.Name)
	}

	return ctx.Reply(response)
}

// CUnassign removes a tag from the calling user
var CUnassign = &drc.Command{
	Name:         "unassign",
	Manual:       []string{"unassign a tag from yourself, read ``.prototype help`` for important notes about the current prototype status"},
	CommandPerms: discordgo.PermissionSendMessages,
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 1,
	},
	Exec: fUnassign,
}

func fUnassign(ctx *drc.Context) error {
	if ctx.Mes.GuildID == "" {
		return ctx.Reply("Direct Messages are unsupported")
	}

	// Parse integer
	id, err := strconv.ParseUint(ctx.Args[0].Raw, 0, 0)
	if err != nil {
		return drc.NewParseError(err, "Invalid numarical ID")
	}

	// get tag as to be able to use the name in response
	tag, err := GetTag(ctx.Mes.GuildID, id)
	if err != nil {
		return drc.NewFailure(err, "Error finding tag")
	}

	// remove tag from user
	err = RemUserTag(ctx.Mes.GuildID, ctx.Mes.Author.ID, id)
	if err != nil {
		return drc.NewFailure(err, "Error assigning tag")
	}

	var response string
	response = fmt.Sprintf("%v unassigned tag %v", ctx.Mes.Author.String(), tag.Name)

	return ctx.Reply(response)
}

// CProfile gets somebodies (or the callers) profile
var CProfile = &drc.Command{
	Name:         "profile",
	Manual:       []string{"view either yours or somebody elses profile, read ``.prototype help`` for important notes about the current prototype status"},
	CommandPerms: discordgo.PermissionSendMessages,
	Config: drc.CfgCommand{
		Listable: true,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 0,
	},
	Exec: fProfile,
}

func fProfile(ctx *drc.Context) error {
	if ctx.Mes.GuildID == "" {
		return ctx.Reply("DM support has not yet been implemented, apologies for the inconvenience")
	}

	if len(ctx.Args) == 1 {
		member, err := drc.ParseMember(ctx.Ses, ctx.Args[0].Raw, ctx.Mes.GuildID)
		if err != nil {
			return err
		}
		return doProfile(ctx.Ses, member.User, member, ctx.Mes.GuildID, ctx.Mes.ChannelID)
	}
	return doProfile(ctx.Ses, ctx.Mes.Author, ctx.Mes.Member, ctx.Mes.GuildID, ctx.Mes.ChannelID)
}

// doProfile generates a profile message and sends it to a target channel
func doProfile(s *discordgo.Session, user *discordgo.User, member *discordgo.Member, guild, destination string) error {
	tags, err := GetUserTagsInfo(guild, user.ID)
	if err != nil {
		return drc.NewFailure(err, "Error fetching tags")
	}

	roles := make([]string, 0, len(member.Roles))
	for _, x := range member.Roles {
		role, err := s.State.Role(guild, x)
		if err != nil {
			return err
		}
		roles = append(roles, role.Name)
	}

	reply := fmt.Sprintf("--- Profile for: **%v**\nRoles:```\n%v\n```Tags:```\n%v\n```",
		user.String(), roles, tags)

	_, err = s.ChannelMessageSend(destination, reply)
	return err
}
