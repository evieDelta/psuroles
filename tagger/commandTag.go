package tagger

import (
	"strconv"

	"codeberg.org/eviedelta/drc"
	"github.com/bwmarrin/discordgo"
)

// CTag is the tag parent command
var CTag = &drc.Command{
	Name:         "tag",
	Manual:       []string{"tag function"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		Discord: discordgo.PermissionManageRoles,
	},
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 0,
	},
	Exec: fTag,
}

// tag just lists its subcommands
func fTag(ctx *drc.Context) error {
	return ctx.Reply(ctx.Com.Subcommands.List(false))
}

// give tag its subcommands
func init() {
	CTag.Subcommands.Add(CTagAdd)
	CTag.Subcommands.Add(CTagGet)
	CTag.Subcommands.Add(CTagGetall)
	CTag.Subcommands.Add(CTagDelete)
}

// CTagAdd adds a tag
var CTagAdd = &drc.Command{
	Name:   "add",
	Manual: []string{"Creates a new tag, first argument is the name"},
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 1,
	},
	Exec: fTagAdd,
}

func fTagAdd(ctx *drc.Context) error {
	tag, err := NewTag(ctx.Mes.GuildID, ctx.Args[0].Raw)
	if err != nil {
		return err
	}

	return ctx.Reply("Added tag: ", ctx.Args[0], "(", tag, ")")
}

// CTagGet gets a perticular tags info by its ID
var CTagGet = &drc.Command{
	Name:   "get",
	Manual: []string{"fetches a tag"},
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 1,
	},
	Exec: fTagGet,
}

func fTagGet(ctx *drc.Context) error {
	i, err := strconv.ParseUint(ctx.Args[0].Raw, 0, 0)
	if err != nil {
		return drc.NewParseError(err, "Invalid ID")
	}

	tag, err := GetTag(ctx.Mes.GuildID, i)
	if err != nil {
		return err
	}

	return ctx.Reply("Got tag: ", tag)
}

// CTagGetall gets all tags in a guild
var CTagGetall = &drc.Command{
	Name:   "getall",
	Manual: []string{"fetches all tags"},
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 0,
	},
	Exec: fTagGetall,
}

func fTagGetall(ctx *drc.Context) error {
	tags, err := GetAllTags(ctx.Mes.GuildID)
	if err != nil {
		return err
	}

	return ctx.Reply("Got tags: ```\n", tags, "\n```")
}

// CTagDelete is a command that deletes a tag
var CTagDelete = &drc.Command{
	Name:   "delete",
	Manual: []string{"deletes a tag"},
	Config: drc.CfgCommand{
		Listable:    true,
		MinimumArgs: 1,
	},
	Exec: fTagDelete,
}

func fTagDelete(ctx *drc.Context) error {
	i, err := strconv.ParseUint(ctx.Args[0].Raw, 0, 0)
	if err != nil {
		return drc.NewParseError(err, "Invalid ID")
	}

	err = DelTag(ctx.Mes.GuildID, i)
	if err != nil {
		return err
	}

	return ctx.Reply("Removed Tag ", i)
}
