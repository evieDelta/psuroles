package tagger

import "github.com/bwmarrin/discordgo"

// GuildCreate initialises a new guild for use
func GuildCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	serverMutex.Lock()
	defer serverMutex.Unlock()

	serverStash[g.ID] = Server{
		Tags: make(map[uint64]*Tag),
	}
}
