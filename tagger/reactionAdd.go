package tagger

import (
	"log"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

// ReactionAdd handles reactions added to messages, but pretty much just serves as a wrapper to handleReaction so that can return error
func ReactionAdd(s *discordgo.Session, r *discordgo.MessageReactionAdd) {
	err := HandleReaction(s, r)
	if err != nil {
		// TODO once logger support is added to le handler
		log.Println("ReactionAdd error", err)
	}
}

// HandleReaction handles reaction, kidding aside it does the checks on whether or not an added reaction is one it should listen to, and calles the handler for that
func HandleReaction(s *discordgo.Session, r *discordgo.MessageReactionAdd) error {
	if r.UserID == s.State.User.ID {
		return nil
	}
	// switch case for hardcoded bois
	switch r.Emoji.APIName() {
	case "❓":
		return reactionProfile(s, r)
	case "❔":
		return reactionProfile(s, r)
	}

	// if the emote has this very specific name it'll also be counted
	if r.Emoji.Name == "__BotProfile" {
		return reactionProfile(s, r)
	}

	return nil
}

// run the reactionprofile generator thing
func reactionProfile(s *discordgo.Session, r *discordgo.MessageReactionAdd) error {
	// fetch the message the reaction was on (appears i have to do this to get the message author)
	// might have to look into caching messages
	message, err := s.ChannelMessage(r.ChannelID, r.MessageID)
	if err != nil {
		return errors.Wrap(err, "discordgo.Session.ChannelMessage")
	}
	// fetch the member data of the author
	member, err := s.State.Member(r.GuildID, message.Author.ID)
	if err != nil {
		return errors.Wrap(err, "discordgo.Session.State.Member")
	}

	// open the dm channel for the user who added the reaction
	ch, err := s.UserChannelCreate(r.UserID)
	if err != nil {
		return errors.Wrap(err, "discordgo.Session.UserChannelCreate")
	}

	// run doProfile with the gathered information
	err = doProfile(s, message.Author, member, r.GuildID, ch.ID)
	if err != nil {
		return errors.Wrap(err, "tagger.doProfile")
	}

	// remove the reaction from the message, returning any error it might provide
	err = s.MessageReactionRemove(r.ChannelID, r.MessageID, r.Emoji.APIName(), r.UserID)
	return errors.Wrap(err, "discordgo.Session.MessageReactionRemove")
}
