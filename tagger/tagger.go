package tagger

import (
	"sync"

	"codeberg.org/eviedelta/drc"
	"git.lan/psuroles/module"
)

// Module be the module that contains the module stuff that does the stuff
var Module = module.Module{
	Commands: []*drc.Command{
		CTag,
		CAssign,
		CUnassign,
		CProfile,
	},
	DgoHandlers: []interface{}{
		GuildCreate,
		ReactionAdd,
	},
}

// temporary storage solution because its a poc and doesn't need persistance, this'd be rewritten if developed past this stage

// Tag contains the information about tag, helpful comment i know
type Tag struct {
	Name string
	ID   uint64
}

// Server just contains what tags the server contains so far
type Server struct {
	Tags map[uint64]*Tag
}

// User lists what tags the user has
type User struct {
	Tags map[uint64]bool
}

// Store servers, mutex is for syncing to prevent race conditions, lock the mutexes prior to making any reads or changes and unlock it afterwards, same with userMutex
var serverMutex sync.Mutex
var serverStash = make(map[string]Server)

// Store users
var userMutex sync.Mutex
var userStash = make(map[string]*User)

// check if a guild+user is unset or not (else it'll panic from a null pointer)
func userNilCheck(guild, user string) {
	key := guild + ":" + user

	if userStash[key] == nil {
		userStash[key] = &User{}
	}

	if userStash[key].Tags == nil {
		userStash[key].Tags = make(map[uint64]bool)
	}
}

// ID generator thing, probably be also replaced with a proper database
var ints = make(chan uint64)

func init() {
	go idThing()
}

func idThing() {
	var i uint64
	for {
		i++
		ints <- i
	}
}
