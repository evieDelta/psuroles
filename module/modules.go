package module

import "codeberg.org/eviedelta/drc"

// Module contains stuff about modules (obviously)
type Module struct {
	Commands []*drc.Command

	DgoHandlers []interface{}
}
