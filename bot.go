package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/drc/subpresets"
	"codeberg.org/eviedelta/trit"
	"git.lan/psuroles/config"
	"git.lan/psuroles/module"
	"github.com/bwmarrin/discordgo"
)

var conf config.Config

// Dg is the main discord session
var Dg *discordgo.Session

// Hn is the global handler
var Hn *drc.Handler

// flags variables for some global configuration
var (
	flagConfig string
)

// this is lazy, i'll rewrite this to be prettier eventually
func launch(modules []module.Module) {
	var err error

	// make a new bot session
	Dg, err = discordgo.New("Bot " + conf.Auth.Token)
	if err != nil {
		log.Fatalln("Error creating discord session", err)
	}

	// initialise handler session
	Hn = drc.NewHandler(drc.CfgHandler{
		Prefixes: conf.Bot.Prefix,
		Admins:   conf.Bot.Admins,

		ReplyOn: drc.ActOn{
			Error:   trit.True,
			Denied:  trit.True,
			Failure: trit.True,
		},
		ReactOn: drc.ActOn{
			Error:   trit.True,
			Denied:  trit.True,
			Failure: trit.True,
		},

		DefaultSubcommands: []drc.Command{
			subpresets.SubcommandSimpleHelp,
		},
	}, Dg)

	// add the core handlers (guild create mostly just exists for debug at this point)
	Dg.AddHandler(onMessage)
	Dg.AddHandler(guildCreate)

	// Load up all the provided modules
	for _, m := range modules {
		// Add all commands
		Hn.Commands.AddBulk(m.Commands)

		// if no extra discordgo handlers were provided skip and contine
		if len(m.DgoHandlers) < 1 {
			continue
		}
		// else add those to the discordgo session
		for _, h := range m.DgoHandlers {
			Dg.AddHandler(h)
		}
	}

	// Initialise the handler
	err = Hn.Ready()
	if err != nil {
		log.Fatalln("Error initialising handler", err)
	}
}

// starts the bot and runs it until something requests it to close
func startUntilStop() {
	// Connect to discord
	err := Dg.Open()
	if err != nil {
		log.Fatalln("Error opening connection", err)
	}
	// Disconnect once everything is done
	defer Dg.Close()

	// Wait until a shutdown signal is recived
	fmt.Println("Bot Running, Press CTRL-C to Shutdown")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

// guildCreate just logs all guilds added for debug purposes
func guildCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	if conf.Bot.Debugm {
		log.Println("guild create: ", g.Name, " (", g.ID, ")")
	}
}
