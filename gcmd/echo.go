package gcmd

import (
	"strings"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

// Echo is an echo command that echos stuff
var Echo = &drc.Command{
	Name:         "echo",
	Manual:       []string{"A basic echo command, will echo anything after it\nFlags```\n-c <channel> | mention a channel to echo to\n-s | prepend the message with <username> says, this is always on if the user using the command lacks manage_channel```"},
	CommandPerms: discordgo.PermissionSendMessages,
	Config: drc.CfgCommand{
		Listable: true,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		BoolFlags: map[string]bool{
			"s": false,
		},
		DataFlags: map[string]string{
			"c": "",
		},
		MinimumArgs: 1,
	},
	Exec: fEcho,
}

func fEcho(ctx *drc.Context) error {
	xs := ctx.BoolFlags["s"]
	ch, ok, err := ctx.Flags["c"].Channel(ctx)

	says := xs
	channel := ctx.Mes.ChannelID

	if ok && err == nil {
		channel = ch.ID
	} else if ok {
		return err
	}

	perm, err := ctx.Ses.UserChannelPermissions(ctx.Mes.Author.ID, channel)
	if drc.PermissionCheck(perm, discordgo.PermissionSendMessages&discordgo.PermissionReadMessages) != 0 {
		return drc.NewDenied(nil, "You lack the permissions for <#"+ch.ID+">")
	}
	if drc.PermissionCheck(perm, discordgo.PermissionManageChannels) != 0 {
		says = true
	}

	msgdata := strings.Join(ctx.RawArgs, " ")

	if says {
		msgdata = ctx.Mes.Author.String() + " says: " + msgdata
	}

	_, err = ctx.Ses.ChannelMessageSend(channel, msgdata)
	return err
}
