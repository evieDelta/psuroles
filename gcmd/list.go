package gcmd

import (
	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

var CList = &drc.Command{
	Name:         "commands",
	Manual:       []string{"lists commands"},
	CommandPerms: discordgo.PermissionSendMessages,
	Config: drc.CfgCommand{
		Listable: true,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 0,
	},
	Exec: fList,
}

func fList(ctx *drc.Context) error {
	return ctx.Reply("```\n", ctx.Han.Commands.List(false), "\n```")
}
