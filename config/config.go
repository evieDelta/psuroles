package config

import (
	"fmt"
	"os"

	"github.com/burntsushi/toml"
)

// Config contains the core configuration for the bot, i mean what else would it contain
type Config struct {
	Auth Auth
	Bot  Bot
}

// Auth contains authentication configuration
type Auth struct {
	Token string
}

// Bot contains some general purpose configuration
type Bot struct {
	Debugm bool
	Prefix []string
	Admins []string
}

// AConf is a function dedicated to checking various locations for a config, and loading that config
func AConf(loc string) (Config, error) {
	var search []string
	if loc == "" {
		search = []string{"./config.toml", "./data/config.toml"}
	} else {
		search = []string{loc, "./config.toml", "./data/config.toml"}
	}

	for _, f := range search {
		if !CheckExist(f) {
			continue
		}
		cfg, err := Load(f)
		return cfg, err
	}

	fmt.Println("No configuration found. locations checked: ", search)

	os.Exit(0)
	return Config{}, nil
}

// Load loads a toml based config file
func Load(loc string) (cfg Config, err error) {
	_, err = toml.DecodeFile(loc, &cfg)
	return
}

// CheckExist is a lazy function to determine if a file exists or not
func CheckExist(loc string) bool {
	i, err := os.Stat(loc)
	if err != nil || i.IsDir() {
		return false
	}
	return true
}

// CheckDirExist is a lazy function to determine if a directory exists or not
func CheckDirExist(loc string) bool {
	i, err := os.Stat(loc)
	if err != nil || !i.IsDir() {
		return false
	}
	return true
}
