module git.lan/psuroles

go 1.14

require (
	codeberg.org/eviedelta/drc v0.0.0-20200611065755-b503e601f9d5
	codeberg.org/eviedelta/trit v0.0.0-20200522120239-e627923758e3
	github.com/burntsushi/toml v0.3.1
	github.com/bwmarrin/discordgo v0.20.3
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
)
