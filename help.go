package main

import (
	"fmt"
	"runtime"
	"time"

	"codeberg.org/eviedelta/drc"
	"git.lan/psuroles/module"
	"github.com/bwmarrin/discordgo"
)

// module containing some help commands
var helpmod = module.Module{
	Commands: []*drc.Command{
		cInfo,
		cHelp,
		cTechnical,
		cPrototype,
	},
}

// the version of the bot
const botVersion = "v0.1.0 prototype"

// bots help message
const botDescription = `A prototype bot that attempts to create a sort of role esk system to provide aesthetic tags without taking up role slots
see 'x-help' for info on the commands and how to use it
Currently highly alpha and only a prototype so some functions may not work as ideally
(see 'x-prototype' for info on the limitations)
Final version may not be built on the same toolset as this is just a prototype`

// some stuff for the help
const botHelp = `to use the bot you can use the commands listed below, and react with :question: on anyone to get their profile`

// handwritten list of commands (yes i have functions to auto generate command lists, but i was lazy and didn't feel like writing the formatting for it)
const botCommands = `
info             | bot info
help             | this
prototype        | some info on the prototypes limitations
technical        | nerd stats

tag add <tag>    | adds a new tag
tag get <tag>    | gets info on a tag
tag getall       | gets a list of all tags (functionally is a list command)
tag delete <tag> | deletes a tag

assign <tag>     | assign a tag to yourself
unassign <tag>   | remove a tag from yourself

profile <?user>  | get the profile of you, or yourself if you specify a user
                 | alternatively you can react with ? on any user to get DM'ed their profile
`

// some info about the limitations of the prototype
const prototype = `
This bot as you see right now is a very early proof of concept, as such some limitations apply to its functionality and use

- 1 - This is a Proof of concept, not a final bot
----- The only functionality present within this bot is the bare minimum to show the concept, and as such many things may be missing

- 2 - Nothing will persist
----- Everything done with the bot will be reset once it restarts

- 3 - The commands within the bot are still test commands
----- As such the way the commands are used may not be as intuitive as what a complete version might have
----- the main and most important example of this are the list, assign, and unassign commands you must use the numarical ID that goes with the tag, the name will not work

- 4 - No customisation or configurability options are present
----- The final version will likely contain much more options for the flairs/tags/etc, and possibly also restrictions to them

- 5 - The bot may crash
----- This is a prototype using prototype code, and along with point 1 things will be lost if it happens

- 6 - The names are not final
----- However for the duration of the prototype "tag" is used to refer to them
----- if you have suggestions for what the 'tags' should be called let us know (current ideas are Tags, titles, marks, flairs, and psudoroles)
`

// info command, displays bot info
var cInfo = &drc.Command{
	Name: "info",
	Exec: func(ctx *drc.Context) error {
		msg := "--- **Prototype Tag/Flair/Title/Psudorole Bot**\n" + botDescription

		return ctx.Reply(msg)
	},
}

// help command siaplays some help
var cHelp = &drc.Command{
	Name: "help",
	Exec: func(ctx *drc.Context) error {
		msg := "--- **Help**\n" +
			botHelp + "```\n" + botCommands + "```"

		return ctx.Reply(msg)
	},
}

// the starting time of the bot, used to get uptime
var startTime = time.Now()

// nerd stats
var cTechnical = &drc.Command{
	Name: "technical",
	Exec: func(ctx *drc.Context) error {
		msg := fmt.Sprintf(
			"--- **Technical Stats**\nLanguage: %v on %v %v\nLibrary: Discordgo %v\nCommand Router Version: %v\nBot Version: %v\nUptime: %v\nPing: %vms",
			runtime.Version(), runtime.GOOS, runtime.GOARCH, discordgo.VERSION, drc.Version, botVersion,
			time.Now().Sub(startTime).Truncate(time.Second).String(),
			ctx.Ses.HeartbeatLatency().Milliseconds(),
		)

		return ctx.Reply(msg)
	},
}

// display prototype
var cPrototype = &drc.Command{
	Name:   "prototype",
	Manual: []string{prototype},
	Exec:   fPrintDescription,
}

// thing that is only used for prototye for some reaons, basically just replies its own description
func fPrintDescription(ctx *drc.Context) error {
	return ctx.Reply(ctx.Com.Manual[0])
}
